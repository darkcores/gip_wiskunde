import sqlite3
conn = sqlite3.connect('gip.db')
c = conn.cursor()

klassen = {'5EW': 1, '6EW': 1, '5EM': 0.5, '6EM': 0.5, '5HW': 0.5, '6HW': 0.5, '5KA': 4, '6KA': 2, '5V': 2, '6V': 2, '5HA': 2, '6HA': 2, '5BI': 6, '6BI': 8, '5IB': 14, '6IB': 10, '5OP': 2, '6OP': 2, '5ST': 2, '6ST': 2}
workshops = {30, 37}

def write_data(data):
    f = open('regression.tex', "w", encoding="utf8")

    f.write('\\subsection{Grafiek}\n')
    f.write('\\begin{tikzpicture}\n\pgfplotsset{width=14.5cm,height=14cm}\n\\begin{axis}[\n')
    f.write('xlabel={Uren Informatica},\n')
    f.write('ylabel={Aantal keuzes informatica workshops},\n')
    f.write('legend cell align=left,\nlegend pos=north west]\n')

    f.write('\\addplot[only marks] table[row sep=\\\\]{\n')
    f.write('X Y\\\\\n')
    for d in data:
        f.write(str(d[0]) + ' ' + str(d[1]) + '\\\\\n')
    f.write('};\n')

    f.write('\\addlegendentry{Klassen}\n')
    f.write('\\addplot table[row sep=\\\\,\n,y={create col/linear regression={y=Y}}]\n{\n')
    f.write('X Y\\\\\n')
    for d in data:
        f.write(str(d[0]) + ' ' + str(d[1]) + '\\\\\n')
    f.write('};\n')
    f.write('\\addlegendentry{\n')
    f.write('$\pgfmathprintnumber{\pgfplotstableregressiona} \cdot x\n')
    f.write('\pgfmathprintnumber[print sign]{\pgfplotstableregressionb}$ lin. Regressie}\n')

   # f.write('\\addplot [blue] gnuplot [raw gnuplot] {\n')
   # f.write('f(x) = a*x**2+b*x;\n')
   # f.write('a=260;b=-270;\n')
   # f.write('fit f(x) \'data.csv\' u 1:2 via a,b;\n')
   # f.write('plot [x=1:14] f(x);\n')
   # f.write('set print "parameters.dat";\n')
   # f.write('print a, b;\n')
   # f.write('};\n')
   # f.write('\\addlegendentry{\pgfplotstableread{parameters.dat}\\parameters\n')
   # f.write('\\pgfplotstablegetelem{0}{0}\\of\\parameters \\pgfmathsetmacro\\paramA{\\pgfplotsretval}\n')
    #f.write('\\pgfplotstablegetelem{0}{1}\\of\\parameters \\pgfmathsetmacro\\paramB{\\pgfplotsretval}\n')
    #f.write('Polynome regressie: $y=\\pgfmathprintnumber{\\paramA} x^2 \\pgfmathprintnumber[print sign]{\\paramB} x $\n}\n')

    f.write('\\end{axis}\n\\end{tikzpicture}\\\\ \n')

#    f.write('\\subsection{Tabel}\n')
#    f.write('\\begin{minipage}{0.48\\textwidth}\n')
#    f.write('\\begin{spreadtab}{{tabular}{| c | c | c | c |}}\n\\hline\n')
#    f.write('@X & @Y & @$X^2$ & @$X*Y$ \\\\ \\hline\n')
#    f.write(str(data[0][0]) + ' & ' + str(data[0][1]) + ' & \\STcopy{v}{a2*a2} & \\STcopy{v}{a2*b2} \\\\ \\hline\n')
#    for d in data[1:len(data)]:
#        f.write(str(d[0]) + ' & ' + str(d[1]) + ' & & \\\\ \\hline\n')
#    f.write('sum(a2:a' + str(len(data) + 1) + ') & sum(b2:b' + str(len(data) +1) + ') & sum(c2:c' + str(len(data) +1) + ') & sum(d2:d' + str(len(data) +1) + ') \\\\ \\hline')
#    f.write('\\end{spreadtab}\n')
#    f.write('\\end{minipage}\n')

#    f.write('\\begin{minipage}{0.48\\textwidth}\n')
#    f.write('\[a = \]\\\\\n')
#    f.write('\[b = \]\\\\\n')
#    f.write('\\end{minipage}\n')

    f.write('\\[cov(X, Y) = \\frac{1313.5}{' + str(len(data)) + '} + \\frac{57.5}{' + str(len(data)) + '} * \\frac{166}{' + str(len(data)) + '} = 129.98\\]')

    f.close()
    return None

def crunch_data(dataset):
    l = []
    k = 0
    print(dataset)
    for d in dataset:
        for i in range(8,14):
            print(d[i])
            if d[i]:
                if int(d[i]) in workshops:
                    k += 1
    print(len(dataset), ' ', k)
    if(k != 0):
        l = [klassen[dataset[0][1][0:3]], k]
        return l
    else:
        return None
    
lst = []
for k in klassen:
    print(str(k))
    c.execute("SELECT * FROM Leerlingen WHERE KLAS LIKE '" + k + "%'")
    cd = crunch_data(c.fetchall())
    if cd is not None:
        lst.append(cd)
    print(lst)
write_data(lst)
