import sqlite3
conn = sqlite3.connect('gip.db')
c = conn.cursor()

c.execute("SELECT * FROM Workshops")
work = c.fetchall()

def write_klassen(data):
    f = open('klassen.txt', "w", encoding="utf8")
    for d in data:
        f.write('\\input{' + d[1] + '.tex}\n')
    f.close()
    return None

def write_file(data, naam, none, minimun, maximum, t_fi):
    f = open(naam + '.tex', "w", encoding="utf8")
    f.write('\\section{' + naam + '}\n')

    f.write('\\subsection{Frequentietabel}\n')
    f.write('\\begin{center}\n')
    f.write('\\begin{tabular}{| c | c | c | c | c | c | c |}\n')
    f.write('\\hline\n\\multicolumn{7}{|c|}{\\textbf{Frequentietabel ' + naam + '}} \\\\ \\hline\n')
    f.write('$i$ & $x_i$ & $f_i$ & $\\varphi _i(\%)$ & $cf_i$ & $c\\varphi _i(\\%)$ & $c\\varphi _i$ \\\\ \\hline\n')
    for d in data:
        for i in range(6):
            f.write(str(d[i]) + ' & ')
        f.write(str(d[6]) + ' \\\\ \\hline \n')
    f.write('\\textbf{Totaal} & \\cellcolor{gray} & ' + str(t_fi) + ' & ' + str(data[-1][5]) + ' & \\cellcolor{gray} & \\cellcolor{gray} & \\cellcolor{gray} \\\\ \\hline \n')
    f.write('\\end{tabular}\n')
    f.write('\\end{center}\n')
    
    f.write("\\begin{landscape}") #------------------
    f.write('\\subsection{Histogram}\n')
    f.write('\\begin{center}\n')
    f.write('\\begin{tikzpicture}\n')
    f.write('\\begin{axis}[\n')
    f.write('ymin=0,\n')
    f.write('ybar interval,\nylabel={Aantal keuzes ' + naam + '},\n')
    f.write('symbolic x coords={')
    i = 0
    for d in data:
        f.write(d[1] + ',')
    f.write('.},\n')
    f.write('x tick label style={rotate=45,anchor=east},\nxmajorgrids=false\n]\n')
    f.write('\\addplot coordinates{')
    for d in data:
        f.write('(' + str(d[1]) + ',' + str(d[2]) + ') ')
    f.write('(.,0)};\n')
    f.write('\\end{axis}\n')
    f.write('\\end{tikzpicture}\n')
    f.write('\\end{center}\n')
    f.write("\\end{landscape}") #------------------

    f.write("\\begin{landscape}") #------------------
    f.write('\\subsection{Frequentie Polygoon}\n')
    f.write('\\begin{center}\n')
    f.write('\\begin{tikzpicture}\n')
    f.write('\\begin{axis}[\n')
    f.write('ymin=0,\n')
    f.write('ylabel={Aantal keuzes ' + naam + '},\nsymbolic x coords={')
    for d in data:
        f.write(d[1] + ',')
    f.write('},\nxtick = {')
    for d in data:
        f.write(d[1] + ',')
    f.write('},\nx tick label style={rotate=45,anchor=east},\n]\n')
    f.write('\\addplot+[sharp plot] coordinates{')
    for d in data:
        f.write('(' + str(d[1]) + ',' + str(d[2]) + ') ')
    f.write('};\n\\end{axis}\n')
    f.write('\\end{tikzpicture}\n')
    f.write('\\end{center}\n')
    f.write("\\end{landscape}") #------------------

    f.write('\\subsection{Hoogste en laagste resultaten}\n')
    if(len(none) > 0):
        f.write('De Workshops zonder keuzes zijn; ')
        for n in none[:-1]:
            f.write(n + ', ')
        f.write(none[-1] + '.')
        f.write('.\\\\ \\\\ \n')
    else:
        f.write('De workshops met de minste keuzes zijn: ')
        for m in minimun[:-1]:
            f.write(m + ', ')
        f.write(minimun[-1] + '.')
        f.write('.\\\\ \\\\ \n')
    f.write('De workshops met de meeste keuzes: ')
    for m in maximum[:-1]:
        f.write(m + ', ')
    f.write(maximum[-1] + '.')
    f.write('.\\\\ \\\\ \n')
    f.write('\\subsection{Besluit}\n')
    f.write("Voor " + naam + ", gaat de voorkeur uit naar de workshop(s): ")
    for m in maximum[:-1]:
        f.write(m + ', ')
    f.write(maximum[-1] + '. ')
    f.write(" Deze workshops hebben dus ook de meeste stemmen gekregen.\n")

    f.write('\\pagebreak\n')
    f.close()
    return None

def count_data(data, naam):
    none = []
    minimun = []
    ming = 1000
    maxg = 0
    t_fi = 0
    maximum = []
    g = 0
    ccount = 0
    v = []
    for w in work:
        w[1].replace('&', '\\&')
        g += 1
        count = 0
        for d in data:
            for i in range(6):
                if(d[i+8] == w[0]):
                    count += 1
        countp = count / (len(data) * 6)
        pct = countp * 100
        ccount += count
        cpct = ccount / (len(data) * 6) * 100
        ccountp = ccount / (len(data) * 6)
        
        dta = [g,w[1].replace('&', '\\&'), count,'%.2f' % pct,'%.0f' % ccount,'%.2f' % cpct,'%.4f' % ccountp]
        v.append(dta)

        t_fi += count

        if (count != 0):
            dta = [g,w[1].replace('&', '\\&'), count,'%.2f' % pct,'%.0f' % ccount,'%.2f' % cpct,'%.4f' % ccountp]
            #v.append(dta)
        else:
            none.append(w[1].replace('&', '\\&'))
        if (count < ming):
            minimun.clear()
            ming = count
        if (count == ming):
            minimun.append(w[1].replace('&', '\\&'))
        if (count > maxg):
            maximum.clear()
            maxg = count
        if (count == maxg):
            maximum.append(w[1].replace('&', '\\&'))
    write_file(v, naam, none, minimun, maximum, t_fi)
    return None

def cklas(klassen):
    for klas in klassen:
        c.execute("SELECT * FROM Leerlingen WHERE Leerlingen.Klas = \'" + klas[1] +"\'")
        count_data(c.fetchall(), klas[1])
    return None

c.execute("SELECT * FROM Klassen WHERE Klas LIKE '5%' OR Klas LIKE '6%'")
write_klassen(c.fetchall())
c.execute("SELECT * FROM Klassen WHERE Klas LIKE '5%' OR Klas LIKE '6%'")
cklas(c.fetchall())

c.execute("SELECT * FROM Leerlingen WHERE KLAS LIKE '1%' OR Klas LIKE '2%'")
count_data(c.fetchall(), "Graad1")
c.execute("SELECT * FROM Leerlingen WHERE KLAS LIKE '3%' OR Klas LIKE '4%'")
count_data(c.fetchall(), "Graad2")
c.execute("SELECT * FROM Leerlingen WHERE KLAS LIKE '5%' OR Klas LIKE '6%'")
count_data(c.fetchall(), "Graad3")
c.execute("SELECT * FROM Leerlingen")
count_data(c.fetchall(), "AlleLeerlingen")

#write_file([[33,'tennis',32,22,54,56,23,64,11],[33,'aardappel',44,22,54,56,33,23,64]], "test")

conn.close()
